<?php
namespace AppBundle\Controller;

use AppBundle\Form\UserType;
use AppBundle\Form\SenderType;
use AppBundle\Form\LoginForm;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Entity\User;
use AppBundle\Entity\Sender;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


class HomeController extends Controller
{

	 /**
     * @Route("/", name="homepage")
     * @Method({"GET","HEAD"})
     */
	public function indexAction(Request $request)
	{
		return $this->render('home/index.html.twig');
	}
//
//	/**
//	 * @Route("/login", name = "login_form")
//	 * @Method({"GET","HEAD"})
//	 */
//	 public function loginAction(Request $request)
//	{
//		$user = new User();
//		$loginForm = $this->createForm(LoginForm::class, $user);
//		return $this->render('home/login.html.twig', array(
//			'loginForm' => $loginForm->createView())
//		);
//	}
//
//	/**
//	 * @param $username string
//	 * @param $password string
//	 */
//	public function ifPasswordIsValid($username, $password)
//	{
//
//		$user = new User();
//		$user->setPassword($password);
//		$password = $this->get('security.password_encoder')->encodePassword($user, $user->getPlainPassword());
//		$repository = $this->getDoctrine()->getRepository('AppBundle:UserAccount');
//
//		$user = $repository->findOneBy(array('username' => $username, 'password' => $password));
//		if ($user)
//		{
//			$user->setUserID($user->getId());
//			return true;
//		}
//		return false;
//	}
//
//	/**
//	 * @Route("/login", name = "user_login")
//	 * @Method({"POST","HEAD"})
//	 */
//	public function loginPostAction(Request $request)
//	{
//		$user =  new User();
//		//$session = $this->get('session');
//
//		$loginForm = $this->createForm(LoginForm::class, $user);
//
//		$loginForm->handleRequest($request);
//		if ($loginForm->isValid()) {
//
//			//get password & id
//			$username = $loginForm->get('username');
//			$password = $loginForm->get('password');
//			$id = 7;
//
//			//check password
//			$passwordIsOK = true;
//			if ( ifPasswordIsValid($username, $password))
//			//if ( $passwordIsOK)
//			{
//				//Session
//				$session = $request->getSession();
//				$session->set("userID", $id);
//				return $this->redirectToRoute('home/index');
//			}
//		}
//		return $this->render('home/login.html.twig', array(
//			'loginForm' => $loginForm->createView(),
//		));
//	}

	/**
	 * @Route("/logout")
	 * @Method({"GET","HEAD"})
	 */
	public function logoutAction(Request $request)
	{
		return $this->render('home/index.html.twig', array(
		));
	}

	/**
	 * @Route("/about")
	 * @Method({"GET","HEAD"})
	 */
	public function aboutAction(Request $request)
	{
		return $this->render('home/about.html.twig');
	}

	/**
	 * @Route("/prices")
	 * @Method({"GET","HEAD"})
	 */
	public function pricesAction(Request $request)
	{
		return $this->render('home/prices.html.twig');
	}

	/**
	 * @Route("/contact")
	 */
	public function contactAction(Request $request)
	{
		return $this->render('home/contact.html.twig');
	}



//	/**
//	 * @Route("/register", name="registration_form")
//	 * @Method("GET")
//	 */
//	public function registerAction(Request $request)
//	{
//		// 1) build the form
//		$user = new User();
//		$sender = new Sender();
//		$userForm = $this->createForm(UserType::class, $user);
//		$senderForm = $this->createForm(SenderType::class, $sender);
//
//		return $this->render('home/form.html.twig', array(
//			'senderForm' => $senderForm->createView(),
//			'userForm' => $userForm->createView(),
//		));
//	}
//
//	/**
//	 * @Route("/register", name="user_registration")
//	 * @Method("POST")
//	 */
//	public function registerPostAction(Request $request)
//	{
//		// 1) build the form
//		$user = new User();
//		$sender = new Sender();
//		$userForm = $this->createForm(UserType::class, $user);
//		$senderForm = $this->createForm(SenderType::class, $sender);
//
//
//		// 2) handle the submit (will only happen on POST)
//		$userForm->handleRequest($request);
//		$senderForm->handleRequest($request);
//		if ( $userForm->isValid() && $senderForm->isValid()) {
//
//			//encode password
//			$password = $this->get('security.password_encoder')
//				->encodePassword($userAccount, $userAccount->getPlainPassword());
//			$userAccount->setPassword($password);
//
//			//save to DB
//			$em = $this->getDoctrine()->getManager();
//
//			$em->persist($sender);
//			$em->flush();
//
//			$user->setUserID($sender);
//			$em->persist($user);
//			$em->flush();
//
//			return $this->redirectToRoute('login_form');
//		}
//
//		return $this->render('home/login.html.twig', array(
//			'senderForm' => senderForm->createView(),
//			'userForm' => userForm->createView(),
//		));
//
//
//	}


}

