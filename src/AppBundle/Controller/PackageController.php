<?php
namespace AppBundle\Controller;

use AppBundle\Form\UserType;
use AppBundle\Form\SenderType;
use AppBundle\Form\LoginForm;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Entity\User;
use AppBundle\Entity\Sender;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


class PackageController extends Controller
{

    /**
     * @Route("/packages", name="packages")
     * @Method({"GET","HEAD"})
     */
    public function viewPackagesAction(Request $request)
    {
        //$user = $this->container->get('fos_user.user_manager')->findUByUsername('admin');
        return $this->render('package/index.html.twig');
    }

    /**
     * @Route("/create", name="create_package")
     * @Method({"GET","HEAD"})
     */
    public function createPackageAction(Request $request)
    {
        //$user = $this->container->get('fos_user.user_manager')->findUByUsername('admin');
        return $this->render('package/create.html.twig');
    }

    /**
     * @Route("/follow", name="follow_package")
     * @Method({"GET","HEAD"})
     */
    public function followPackagesAction(Request $request)
    {
        //$user = $this->container->get('fos_user.user_manager')->findUByUsername('admin');
        return $this->render('package/follow.html.twig');
    }
}